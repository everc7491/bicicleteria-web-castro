
CREATE DATABASE IF NOT EXISTS `bicicleteria` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bicicleteria`;

CREATE TABLE `personas` (
  `rol` varchar(30) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `password` varchar(12) NOT NULL,
  `usuario` varchar(20) NOT NULL UNIQUE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `piezas` (
  `pieza` varchar(30) NOT NULL,
  `codigo` varchar(30) NOT NULL,
  `cantidad` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLA `bicicletas` (
  `codigo` varchar(30) NOT NULL,
  `cantidad` int NOT NULL

)ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `personas` (`rol`, `nombre`, `usuario`, `password`) VALUES
('bicicletero', 'Matías', 'b','1'),
('encargado', 'Gonzalo', 'e','1'),
('vendedor', 'Diego', 'v', '1');

INSERT INTO `bicicletas` (`codigo`, `cantidad`) VALUES
('123', '3');