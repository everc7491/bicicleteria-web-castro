
package bicicleteria;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ModeloStockBici {
     
    private String jdbcDriver;
    private String dbName;
    private String urlRoot;
    private ActionListener listener;
    private List<String> bicis;

    public ModeloStockBici() {
        jdbcDriver = "com.mysql.cj.jdbc.Driver";
        urlRoot = "jdbc:mysql://127.0.0.1/";
        dbName = "bicicleteria";
        listener = null;
        bicis = new ArrayList<>();
        
        try {
            Class.forName(jdbcDriver);
        } catch (ClassNotFoundException e) {
            reportException(e.getMessage());
        }
    }

     public void alta(String codigo) {
        try {
            Connection con = DriverManager.getConnection(urlRoot + dbName, "cualquiera", "");
            Statement stmt = con.createStatement();
            stmt.executeUpdate("INSERT INTO bicicletas (codigo,cantidad) VALUES('"+codigo+"','1');");
            stmt.close();
        } catch(SQLException ex) {
            reportException(ex.getMessage());
        }
    }
    
     public void listarBicis(){
        
       
        try{
            Connection co = DriverManager.getConnection(urlRoot + dbName, "cualquiera", "");
            Statement stmt = co.createStatement();
            stmt.execute("SELECT codigo,cantidad FROM bicicletas;");
            ResultSet rs = stmt.getResultSet();
            
            while(rs.next()){
                String cod = rs.getString("codigo");
                
                  int cant= rs.getInt("cantidad");
            
                      bicis.add(cod);
                  
            }
               
        } catch (SQLException ex){
            reportException(ex.getMessage());
        }
          
     }
     
     
       public boolean venderBici(String c){
        
         try{
            Connection conn = DriverManager.getConnection(urlRoot + dbName, "cualquiera", "");
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM `bicicletas`;");
            
            while(rs.next()){
                String cod = rs.getString("codigo");
                
                  int cant= rs.getInt("cantidad");
                  if(cant>0 && cod == c){
                      cant= cant-1;
                     Statement stm = conn.createStatement();
                     stm.executeUpdate("UPDATE bicicletas SET cantidad='" +cant+ "' WHERE codigo='" +c+ "';");
            
                     
                  }
            }
              return true; 
        } catch (SQLException ex){
            reportException(ex.getMessage());
            return false;
        }
     
      
       
     }
        public void addExceptionListener(ActionListener listener) {
        this.listener = listener;
    }

    private void reportException(String exception) {
        if (listener != null) {
            ActionEvent evt = new ActionEvent(this, 0, exception);
            listener.actionPerformed(evt);
        }
    }

    public List<String> getBicis() {
        return bicis;
    }

}
