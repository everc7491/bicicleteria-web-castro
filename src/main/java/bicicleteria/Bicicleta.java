
package bicicleteria;

public class Bicicleta {
    private String codigo;
    private boolean asiento;
    private boolean kitMecanico;
    private boolean rueda1;
    private boolean rueda2;
    private boolean cuadro;
    private boolean manubrio;
    private boolean pedal1;
    private boolean pedal2;
    private int cantidad;
  

    public Bicicleta(String codigo) {
        this.codigo = codigo;
        setAsiento(false);
        setKitMecanico(false);
        setRueda1(false);
        setRueda2(false);
        setCuadro(false);
        setManubrio(false);
        setPedal1(false);
        setPedal2(false);
        
    }

    public boolean bicicletaCompleta (){
        if(asiento && kitMecanico && rueda1 && rueda2 && cuadro && manubrio && pedal1 && pedal2){
            return true;
        }else{
            return false;
        }
    }
    public void setAsiento(boolean asiento) {
        this.asiento = asiento;
    }

    public void setKitMecanico(boolean kitMecanico) {
        this.kitMecanico = kitMecanico;
    }

    public void setRueda1(boolean rueda1) {
        this.rueda1 = rueda1;
    }

    public void setRueda2(boolean rueda2) {
        this.rueda2 = rueda2;
    }

    public void setCuadro(boolean cuadro) {
        this.cuadro = cuadro;
    }

    public void setManubrio(boolean manubrio) {
        this.manubrio = manubrio;
    }

    public void setPedal1(boolean pedal1) {
        this.pedal1 = pedal1;
    }

    public void setPedal2(boolean pedal2) {
        this.pedal2 = pedal2;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public boolean isAsiento() {
        return asiento;
    }

    public boolean isKitMecanico() {
        return kitMecanico;
    }

    public boolean isRueda1() {
        return rueda1;
    }

    public boolean isRueda2() {
        return rueda2;
    }

    public boolean isCuadro() {
        return cuadro;
    }

    public boolean isManubrio() {
        return manubrio;
    }

    public boolean isPedal1() {
        return pedal1;
    }

    public boolean isPedal2() {
        return pedal2;
    }
}
