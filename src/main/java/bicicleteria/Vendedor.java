package bicicleteria;

import java.util.ArrayList;
import java.util.List;

public class Vendedor extends Persona {
 
   private List<String> bicis;
 
   public Vendedor(List<String> bicis, String usuario, String password, String nombre) {
        super(usuario, password, nombre);
        this.bicis = bicis;
    }
    @Override
    public String getVista() {
        return "vendedor.jsp";
    }

    @Override
    public Object getRecursos() {
        
    return getBicis();
   //  return "123";
    }
 

    public List<String> getBicis() {
        return bicis;
    }

    public void setBicis(List<String> bicis) {
        this.bicis = bicis;
    }
    
}
