package bicicleteria;

import java.util.List;

public class Bicicletero extends Persona {

 private Object piezas;   
    
    public Bicicletero(Object piezas,String usuario, String password, String nombre) {
        super(usuario, password, nombre);
        this.piezas = piezas;
    }

    @Override
    public String getVista() {
        return "bicicletero.jsp";
    }

    @Override
    public Object getRecursos() {
    
      return getPiezas();
    }

    public Object getPiezas() {
        return piezas;
    }
    
}
