package bicicleteria;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ModeloStockPieza {
    
    private String jdbcDriver;
    private String dbName;
    private String urlRoot;
    private ActionListener listener;
    private List<Bicicleta> bicicletas;
    private List<String> piezas;

    public ModeloStockPieza() {
        jdbcDriver = "com.mysql.cj.jdbc.Driver";
        urlRoot = "jdbc:mysql://127.0.0.1/";
        dbName = "bicicleteria";
        listener = null;
        piezas = new ArrayList<>();
        bicicletas = new ArrayList<>();
        try {
            Class.forName(jdbcDriver);
        } catch (ClassNotFoundException e) {
            reportException(e.getMessage());
        }
    }

    public void altaP(String pieza, String codigo, int cantidad) {
        try {
            Connection con = DriverManager.getConnection(urlRoot + dbName, "cualquiera", "");
            Statement stmt = con.createStatement();
            stmt.executeUpdate("INSERT INTO piezas(pieza,codigo,cantidad) VALUES('"+pieza+"','"+codigo+"','"+cantidad+"');");
            stmt.close();
        } catch(SQLException ex) {
            reportException(ex.getMessage());
        }
    }
    public List<String> listaCod(){ //crea lista con todos los codigos ingresados
          List<String> codigos = new ArrayList<>();
    
        try{
            Connection co = DriverManager.getConnection(urlRoot + dbName, "cualquiera", "");
            Statement stmt = co.createStatement();
           
            ResultSet rs = stmt.executeQuery("SELECT * FROM piezas");
        
              while(rs.next()){
                String cod = rs.getString("codigo");
                if(codigos != null){
                    for(String a : codigos){
                        if(!a.equals(cod)){
                            codigos.add(a);
                        }
                    }
                }else{
                    codigos.add(cod);
                }
            }
             
        } catch(SQLException ex){
            reportException(ex.getMessage());
        }
        return codigos;
    }
    
    
    public Object listarPosiblesB(){
        
        List<String> codigos = new ArrayList<>();
        int cantMinima=0;
        if(listaCod() != null){
            for ( String cod : listaCod()){
               try{
                    Connection co = DriverManager.getConnection(urlRoot + dbName, "cualquiera", "");
                    Statement stmt = co.createStatement();
             
                    ResultSet rs = stmt.executeQuery("SELECT * FROM piezas WHERE codigo= '"+cod+"'");
               
                    String pieza= rs.getString("pieza");
                    int c = rs.getInt("cantidad");
                    Bicicleta b = new Bicicleta(cod);
                      while(rs.next()){
                          switch(pieza){
                               case "Manubrio":
                                  b.setManubrio(true);
                                  break;
                               case "KitMecanico":
                                  b.setKitMecanico(true);
                                  break;
                               case "Cuadro":
                                  b.setCuadro(true);
                                  break;
                               case "Pedal":
                                  if(c>1){
                                       b.setPedal1(true);
                                       b.setPedal2(true);
                                       c= c/2;
                                       }
                                  break;
                                case "Rueda":
                                   if(c>1){
                                        b.setPedal1(true);
                                        b.setPedal2(true);
                                        c=c/2;
                                     }
                                    break;
                             case "Asiento":
                                 b.setAsiento(true);
                               
                      
                  }
                /*  if(c > cantMinima){
                  cantMinima = c;
                          }*/
                  }
              
            if( b.bicicletaCompleta()){
                      codigos.add(cod);
                   }  
        } catch(SQLException ex){
            reportException(ex.getMessage());
        }
                  
            }
           
        }
        return codigos;
    }
   
    public void actualizarPiezas(Bicicleta b){
        int cant;
         try{
                Connection co = DriverManager.getConnection(urlRoot + dbName, "cualquiera", "");
                Statement stmt = co.createStatement();
               
             ResultSet rs = stmt.executeQuery("SELECT * FROM piezas WHERE codigo= '"+b.getCodigo()+"';");
                   
             while(rs.next()){
                 String pi = rs.getString("pieza");
                 if(rs.getString("pieza").equals("Pedal") || rs.getString("pieza").equals("Rueda")){
                     cant=2;
                 
                  }else{
                     cant=1;
                 }
                 int aux = rs.getInt("cantidad") - cant;
                     stmt.executeUpdate("UPDATE piezas SET cantidad='" +aux+ "' WHERE codigo='" +b.getCodigo()+ "' AND pieza='"+pi+"' ;");
             }                      
               } catch(SQLException ex){
            reportException(ex.getMessage());
        }  
    }
    
    
     public void listarPiezas(){
        
       
        try{
            Connection co = DriverManager.getConnection(urlRoot + dbName, "cualquiera", "");
            Statement stmt = co.createStatement();
            stmt.execute("SELECT (codigo,cantidad,pieza) FROM piezas;");
            ResultSet rs = stmt.getResultSet();
            
            while(rs.next()){
                String cod = rs.getString("codigo");
                String pieza = rs.getString("pieza");
                  int cant= rs.getInt("cantidad");
            
                      piezas.add(pieza+"("+cod+")");
                  
            }
               
        } catch (SQLException ex){
            reportException(ex.getMessage());
        }
          
     }
    
    public void addExceptionListener(ActionListener listener) {
        this.listener = listener;
    }

    private void reportException(String exception) {
        if (listener != null) {
            ActionEvent evt = new ActionEvent(this, 0, exception);
            listener.actionPerformed(evt);
        }
    }

    public List<String> getPiezas() {
        return piezas;
    }
}
