<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Bicicleteria</title>
  </head>
  <body>
    <h1>VENDEDOR</h1>
    <p>
      Hola ${nombre}!
    </p>
    
    <p>  Listado de bicicletas</p>
    
    
        <form method="post" action="ControladorBici">
            <select name="codigoVender">
                <c:forEach var="codigo" items="${recursos}">
                    <option value="${codigo}">${codigo}</option> 
                </c:forEach>
            </select>
        </form>
     <input type="submit" value="Vender">
       
        <p>Seleccione el codigo para vender</p>
    <form method="post" action="ControladorLogout">
      <input type="submit" value="Salir" >
    </form>
  </body>
</html>