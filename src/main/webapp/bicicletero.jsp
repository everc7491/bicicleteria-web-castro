<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Bicicleteria</title>
  </head>
  <body>
    <h1>Bicicletero</h1>
    <p>
      Hola ${nombre}!
    </p>

    <form method="post" action="ControladorStockPiezas">
      <select name="codigoBici">
        <c:forEach var="codigo" items="${recursos}">
            <option value="${codigo}">${codigo}</option>
        </c:forEach>
      </select>
    
        
      <p><label>cantidad</label><br><input type="text" name="cantidad"></p>
      <input type="submit" value="Armar">
    </form>

    <form method="post" action="ControladorLogout">
      <input type="submit" value="Salir" >
    </form>
  </body>
</html>
